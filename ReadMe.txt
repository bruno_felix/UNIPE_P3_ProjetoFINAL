----------------------------------------------------------------------------
----------------------SPI P3 PROJETO FINAL - 3� EST�GIO---------------------
------------------------------Sistema Acad�mico-----------------------------
----------------------------------------------------------------------------


FERRAMENTAS DE GERENCIAMENTO
----------------------------------------------------------------------------
Trello
Git e GitLab
Dropbox


FERRAMENTAS DE DESENVOLVIMENTO
----------------------------------------------------------------------------
IDE						- NetBeans
SGBDs 						- MySQL Workbentch / Navicat
An�lise do Sistema (Diagramas) 			- StarUML 					
Documenta��o					- Office 2016
MockUps de Tela 				- MockupBuilder


TECNOLOGIAS E BIBLIOTECAS UTILIZADAS
----------------------------------------------------------------------------
Linguagem de Programa��o 	- Java SE 8
Interface GUI 			- JavaFX
APIs				- iText e JavaMail
Criptografia 			- SHA2 - 256bits
Persist�ncia de Dados 		- JPA/Hibernate
Banco de Dados  		- MySQL


EQUIPE
----------------------------------------------------------------------------
Bruno Felix 	- Analista, GP, DBA e Desenvolvedor
Emerson Lemos 	- Design de Interface e Desenvolvedor
Jo�o Marcus 	- Analista de Testes e Desenvolvedor


MATERIAIS DE ESTUDOS E REFER�NCIAS
----------------------------------------------------------------------------
Java para Desenvolvimento Web 				- Caelum
Laborat�rio Java com Testes, XML e Design Patterns 	- Caelum
Livro JavaFX 			 			- Casa do C�digo
Java para Iniciantes (JavaFX)				- Oracle
UML 2 Guia Pr�tico 					- Novatec